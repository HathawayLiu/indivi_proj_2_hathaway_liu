use actix_files::Files;
use actix_web::{web, App, HttpResponse, HttpServer, Responder, middleware};
use rand::Rng;
use std::sync::Mutex;
use serde_json::json;
use std::collections::HashMap;

struct AppState {
    money: Mutex<i32>, // Use Mutex for safe concurrent access
}

// Dice rolling and gambling function
async fn gamble(data: web::Data<AppState>, guess: web::Query<HashMap<String, String>>) -> impl Responder {
    let mut rng = rand::thread_rng();
    let dice_sum: i32 = (0..6).map(|_| rng.gen_range(1..=6)).sum();
    let target: i32 = rng.gen_range(6..=36);
    let guess = guess.get("guess").unwrap_or(&"larger".to_string()).to_lowercase();
    let mut money = data.money.lock().unwrap();
    
    let win = if guess == "larger" {
        dice_sum > target
    } else {
        dice_sum < target
    };

    if win {
        *money += 10;
    } else {
        *money -= 10;
    }

    HttpResponse::Ok().json(json!({
        "dice_sum": dice_sum,
        "target": target,
        "win": win,
        "money": *money,
    }))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let data = web::Data::new(AppState {
        money: Mutex::new(100),
    });

    HttpServer::new(move || {
        App::new()
            .app_data(data.clone())
            .route("/gamble", web::get().to(gamble))
            .service(Files::new("/", "./static").index_file("index.html"))
            .wrap(middleware::Logger::default()) // Adding logging
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
