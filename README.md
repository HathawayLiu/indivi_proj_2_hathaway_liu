# Personal Project 2: Gambling Dice Roller [Advanced Arctix Web App]

## Project Description
This is a project that uses `Rust` to create a simple Arctix Web App. Typically, it requires to containerize advanced Rust Actix web app, build a docker image, and run container locally. To run the container locally, it is required to download `Docker Desktop`.

## Function and Web App details
For this web app, I wrote a rust function in `main.rs` that updates the app in `Mini Project 4` and create a simulated Gambling App with the following functionality:
1. Guess your dice rolling result is larger/smaller than Player 2's rolling.
2. Press the `Roll Dice` button to roll 6 dices at a time. Player 2 will roll those in the same time. A dashboard with results will pop up on the App. If you guess the right result, it will shows that you win.
3. There will be a message showed up when you enter the app, which telling you that you initially have $100 on your balanced. Every time you lose or win, your balance will change.

## Steps
1. Open terminal, use `cargo new <YOUR PROJECT NAME>` to create a new rust project.
2. Go to `/src/main.rs`, write you own function to create your web app. Remember to include `use actix_web::{web, App, HttpResponse, HttpServer, Responder}` and `use actix_files::Files` on top of the file.
3. For a better UI design for the website, create a `static` folder and create a new `index.html` under this folder. Write necessary html code for CSS styling. For this own project, I add several CSS styling to the pop up modal and main page. 

    Go back to your `main.rs`, include `.service(Files::new("/", "./static").index_file("index.html"))` under `App::new()` to make sure the styling apply to the web app.
4. In `Cargo.toml`, include the following dependencies (or more depends on your own app):
    - `actix-web`: web framework for Rust
    - `actix-files`: handle serving static files
    - `rand`: generating random numbers in Rust
5. Create a corresponding `Dockerfile` and `Makefile`. You can simply copy mine, but make sure that the name of project for `Dockerfile` and image name in `Makefile` match the `name` in `Cargo.toml`.
6. Make sure your `Docker Desktop` is open. Open a terminal, cd to your current directory, and run `cargo build` and `cargo run`. Open another terminal in the same time, run `docker build -t <YOUR IMAGE NAME> .` You should be able to see your web app under `http://localhost:8080/`
7. Finally run `docker run -d -p 8080:8080 <YOUR IMAGE NAME>`. Open `Docker Desktop`, under your `Container` you should be able to open the `Port`.

## Web App and Docker Container Screenshot
- Docker Container Screenshot
![Docker](https://gitlab.com/HathawayLiu/indivi_proj_2_hathaway_liu/-/wikis/uploads/1337bb0f91e32501a5e1368c76606bbc/Screenshot_2024-03-20_at_7.23.08_PM.png)
- Web App Screenshot
![Website 1](https://gitlab.com/HathawayLiu/indivi_proj_2_hathaway_liu/-/wikis/uploads/582915df0b3640ad4457dfd079d647e3/Screenshot_2024-03-20_at_7.23.33_PM.png)
![Website 2](https://gitlab.com/HathawayLiu/indivi_proj_2_hathaway_liu/-/wikis/uploads/c0fae7f75bfdc765d0632e2bc2ce4d86/Screenshot_2024-03-20_at_7.23.40_PM.png)
![Website 3](https://gitlab.com/HathawayLiu/indivi_proj_2_hathaway_liu/-/wikis/uploads/3709e157709c9d61adc1462c05dcba68/Screenshot_2024-03-20_at_7.23.46_PM.png)

## Demo Video
![Demo Video](https://gitlab.com/HathawayLiu/indivi_proj_2_hathaway_liu/-/wikis/uploads/c0f5261d983c5034faacfcae93a6c01b/video1278769116.mp4)